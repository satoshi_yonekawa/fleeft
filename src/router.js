import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/views/Home';
import Top from '@/views/Top';

Vue.use(Router)

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      component: Home
    },
    {
        path: '/top',
        component: Top
    }
  ]
})